﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GTA;
using GTA.Native;

namespace RelationAndRiot
{
    public class Main : Script
    {
        private readonly Keys openMenu;
        private int idleTimeout;

        public Main()
        {
            openMenu = Settings.GetValue("Main", "OpenMenuKey", Keys.F6);
            Menus.CreateMenus();
            this.Tick += Main_Tick;
        }

        private void Main_Tick(object sender, EventArgs e)
        {
            if (Game.IsKeyPressed(openMenu) && idleTimeout == 0)
            {
                Menus.menu.Visible = !Menus.menu.Visible;
                idleTimeout = 5;
            }

            if (idleTimeout > 0)
            {
                idleTimeout--;
            }

            Menus.Pool.Process();
        }
    }
}
