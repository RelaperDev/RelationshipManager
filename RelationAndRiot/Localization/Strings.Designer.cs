﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace RelationAndRiot.Localization {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RelationAndRiot.Localization.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   重写当前线程的 CurrentUICulture 属性，对
        ///   使用此强类型资源类的所有资源查找执行重写。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找类似 Default 的本地化字符串。
        /// </summary>
        internal static string ItemDefault {
            get {
                return ResourceManager.GetString("ItemDefault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Default behavior. 的本地化字符串。
        /// </summary>
        internal static string ItemDefaultSummary {
            get {
                return ResourceManager.GetString("ItemDefaultSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Dislike 的本地化字符串。
        /// </summary>
        internal static string ItemDislike {
            get {
                return ResourceManager.GetString("ItemDislike", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Peds from Group A will more likely to get provoked by peds from Group B. 的本地化字符串。
        /// </summary>
        internal static string ItemDislikeSummary {
            get {
                return ResourceManager.GetString("ItemDislikeSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Group A 的本地化字符串。
        /// </summary>
        internal static string ItemGroupA {
            get {
                return ResourceManager.GetString("ItemGroupA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Group B 的本地化字符串。
        /// </summary>
        internal static string ItemGroupB {
            get {
                return ResourceManager.GetString("ItemGroupB", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Selects a group. 的本地化字符串。
        /// </summary>
        internal static string ItemGroupSummary {
            get {
                return ResourceManager.GetString("ItemGroupSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Hate 的本地化字符串。
        /// </summary>
        internal static string ItemHate {
            get {
                return ResourceManager.GetString("ItemHate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Peds from Group A will attack peds in Group B on sight. 的本地化字符串。
        /// </summary>
        internal static string ItemHateSummary {
            get {
                return ResourceManager.GetString("ItemHateSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Like 的本地化字符串。
        /// </summary>
        internal static string ItemLike {
            get {
                return ResourceManager.GetString("ItemLike", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Prevent peds in Group A from attacking peds in Group B, even when being attacked by peds in Group B. 的本地化字符串。
        /// </summary>
        internal static string ItemLikeSummary {
            get {
                return ResourceManager.GetString("ItemLikeSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Neutral 的本地化字符串。
        /// </summary>
        internal static string ItemNeutral {
            get {
                return ResourceManager.GetString("ItemNeutral", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Prevent peds in Group A from attacking Group B unless being attacked by peds in Group B. 的本地化字符串。
        /// </summary>
        internal static string ItemNeutralSummary {
            get {
                return ResourceManager.GetString("ItemNeutralSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 None 的本地化字符串。
        /// </summary>
        internal static string ItemNone {
            get {
                return ResourceManager.GetString("ItemNone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Clears relationship from Group A to Group B and set their behavior to Neutral. This way allows players in Group A to attack peds in Group B. 的本地化字符串。
        /// </summary>
        internal static string ItemNoneSummary {
            get {
                return ResourceManager.GetString("ItemNoneSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Respect 的本地化字符串。
        /// </summary>
        internal static string ItemRespect {
            get {
                return ResourceManager.GetString("ItemRespect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Peds in Group A will protect peds in Group B. 的本地化字符串。
        /// </summary>
        internal static string ItemRespectSummary {
            get {
                return ResourceManager.GetString("ItemRespectSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Riot Mode 的本地化字符串。
        /// </summary>
        internal static string ItemRiotMode {
            get {
                return ResourceManager.GetString("ItemRiotMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Sets whether the game will equip weapons on peds and make them fight each other. Relationships set before riot enabled will be ignored, but set after riot enabled will be respected. 的本地化字符串。
        /// </summary>
        internal static string ItemRiotModeSummary {
            get {
                return ResourceManager.GetString("ItemRiotModeSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 MAIN MENU 的本地化字符串。
        /// </summary>
        internal static string MenuSubtitle {
            get {
                return ResourceManager.GetString("MenuSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Relationship 的本地化字符串。
        /// </summary>
        internal static string MenuTitle {
            get {
                return ResourceManager.GetString("MenuTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 ~h~NOTE~w~: If you wish to ~b~let two groups have same ~w~relationship each other you&apos;ll have to ~b~reverse group A and B~w~ and set again. 的本地化字符串。
        /// </summary>
        internal static string SetMessage {
            get {
                return ResourceManager.GetString("SetMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Failed to set relationship. Please try again. 的本地化字符串。
        /// </summary>
        internal static string SetTryAgain {
            get {
                return ResourceManager.GetString("SetTryAgain", resourceCulture);
            }
        }
    }
}
