﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RelationAndRiot.Serialization
{
    [Serializable]
    [XmlType("Group")]
    public class XmlRelationshipGroup
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("display")]
        public string DisplayName { get; set; }
    }
}
