﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RelationAndRiot.Serialization
{
    [Serializable]
    [XmlType("RelationshipManager")]
    public class XmlGroupDocument
    {
        [XmlAttribute("version")]
        public int ManifestVersion { get; set; }

        public XmlRelationshipGroup[] Groups;
    }
}
