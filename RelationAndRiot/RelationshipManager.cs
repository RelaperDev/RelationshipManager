﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using GTA.UI;
using RelationAndRiot.Serialization;

namespace RelationAndRiot
{
    internal static class RelationshipManager
    {
        internal static readonly Dictionary<string, string> RelationshipGroups = new Dictionary<string, string>();

        internal static string[] GetRelationshipGroupKeys()
        {
            return RelationshipGroups.Keys.ToArray();
        }

        static RelationshipManager()
        {
            var serializer = new XmlSerializer(typeof(XmlGroupDocument));
            XmlGroupDocument document;
            using (var stream = File.OpenRead("scripts\\Relationship Manager.xml"))
            {
                document = (XmlGroupDocument)serializer.Deserialize(stream);
            }

            if (document.ManifestVersion != 1)
            {
                // WAI - nobody calls this
#pragma warning disable S3877 // Exceptions should not be thrown from unexpected methods
                throw new InvalidOperationException("Invalid relationship group XML version - extract a new one from install archive!!!");
#pragma warning restore S3877 // Exceptions should not be thrown from unexpected methods
            }

            foreach (var item in document.Groups)
            {
                RelationshipGroups.Add(item.DisplayName, item.Id);
            }

            Notification.Show("Relationship Manager Ready!");
        }
    }
}
