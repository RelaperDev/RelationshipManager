# Relationship Manager
[![Build status](https://ci.appveyor.com/api/projects/status/ij38akkwrdhqcliv?svg=true)](https://ci.appveyor.com/project/WithLithum/relationshipmanager)

Relationship Manager, codenamed RelationAndRiot, is a Grand Theft Auto V script (running on [Community Script Hook V .NET](https://github.com/crosire/scripthookvdotnet) API v3, with [LemonUI](https://github.com/justalemon/LemonUI)).

## Features
Task items left unchecked are either not implmented yet, or is being implmented.

* [x] Modifies Relationship between groups
* [x] Enable/Disable leftover Riot Mode
* [x] Customizable relationship groups
* [ ] Relationship group presets reading
* [ ] Relationship group presets saving

## Contributing
1. **Fork** this repository.
2. Commit changes in your fork. You'll need to commit **as least a working PoC**.
3. Open a merge request [here](https://gitlab.com/RelaperDev/RelationshipManager/-/merge_requests/new).
4. Commit more changes if neccessary.
5. Await until review and/or merge.

## License
This project is licensed under [Apache-2.0](LICENSE) license.

